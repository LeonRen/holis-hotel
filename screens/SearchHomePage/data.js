const tempObjects = [
  {
    title: "Majestic Hotel",
    text: "The Majestic has long been a place",
    priceText: "21$ /night",
    rating: 5,
    image: require("../../assets/images/photos/2.jpeg")
  },
  {
    title: "H10 Cubik",
    text: "This vibrant boutique hotel",
    priceText: "25$ /night",
    rating: 4.2,
    image: require("../../assets/images/photos/3.jpeg")
  },
  {
    title: "B-Hotel",
    text: "This contemporary three-star hotel",
    priceText: "30$ /night",
    rating: 5,
    image: require("../../assets/images/photos/4.jpeg")
  },
  {
    title: "Almanac Hotel",
    text: "The essence of luxury travel",
    priceText: "30$ /night",
    rating: 5,
    image: require("../../assets/images/photos/5.jpeg")
  },
  {
    title: "Soho House",
    text: "One imagines this is how it feels",
    priceText: "19$ /night",
    rating: 2.5,
    image: require("../../assets/images/photos/1.jpeg")
  }

];
const RECS = [
  {
    title: "Terme",
    rating: 4.4,
    hotelsNum: 238,
    source: require("../../assets/images/photos/1.jpeg")
  },
  {
    title: "Caelimontium",
    rating: 4.9,
    hotelsNum: 332,
    source: require("../../assets/images/photos/2.jpeg")
  },
  {
    title: "Pincio",
    rating: 4.1,
    hotelsNum: 312,
    source: require("../../assets/images/photos/3.jpeg")
  },
  {
    title: "Marte",
    rating: 4.9,
    hotelsNum: 431,
    source: require("../../assets/images/photos/4.jpeg")
  },
  {
    title: "Flaminio",
    rating: 4.2,
    hotelsNum: 212,
    source: require("../../assets/images/photos/5.jpeg")
  }
];
export {RECS};
export {tempObjects};
