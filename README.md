### React Native Holistic Hotel App Theme v1.0.0

Thanks for purchasing the React Native Holistic Hotel App Theme.

Follow the documentation to install and get started with the development:

* [Documentation](https://wondernetwork.gitbook.io/holistichoteluikit/)
* [Product Page](https://market.nativebase.io/view/react-native-holistic-hotel-app-theme)

Happy coding!